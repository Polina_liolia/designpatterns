﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod.Pattern
{
    public class ConcretCreatorA : Creator
    {
        public override Product FactoryMethod()
        {
            return new ConcretProductA() { Name = "Product A" };
        }
    }
}
