﻿using FactoryMethod.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            ConcretCreatorA creatorA = new ConcretCreatorA();
            Product productA = creatorA.FactoryMethod();
            Console.WriteLine(productA.Name);

            ConcretCreatorB creatorB = new ConcretCreatorB();
            Product productB = creatorB.FactoryMethod();
            Console.WriteLine(productB.Name);

            Console.WriteLine("Press any key...");
            Console.ReadKey();

        }
    }
}
