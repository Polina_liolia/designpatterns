﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OrderDeliveryBuilder.Pattern.ConcreteBuilder;

namespace OrderDeliveryBuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            Client client = new Client() { Name = "Petr", Phone = "0506789457" };
            //из корзины
            Basket basket1 = new Basket()
            {
                Client = client,
                Products = new List<Product>()
                {
                    new Product(){Name = "Product1", Price = 100.2},
                    new Product(){Name = "Product2", Price = 24.22}
                }
            };
            Order order1 = new Order(basket1);
            order1.Caption = "Order1";

            //
            Basket basket2 = new Basket()
            {
                Client = client,
                Products = new List<Product>()
                {
                    new Product(){Name = "Product3", Price = 88.40},
                    new Product(){Name = "Product4", Price = 12.89}
                }
            };
            Order order2 = new Order(basket2);
            order2.Caption = "Order2";
           
            //
            Delivery delivery = new Delivery();
            Manager manager = new Manager(delivery, "20/04/2018", "по месту жительства Клиента", client.Name);
            manager.addToDelivery(order1);
            manager.addToDelivery(order2);
            //метод addToDelivery должен быть сложным по
            //структуре, тогда оправдано использование
            //паттерна Builder
            //как правило ключевой метод паттерна Builder
            //сам реализуется по патерну Template Method
            delivery = manager.getCompletedDelivery();
            Console.WriteLine(delivery.ToString());
            //Д.З.:
            //1. Избавиться от иллюстрирующих понятия "Builder"
            //ролевых классов Builder, Director, Product
            //2. Ввести остальные смысловые классы:
            //Client - покупатель имя, телефон
            //Product = Товар, который купил клиент (имя цена)
            //Basket = корзина: список товаров и Клиент
            //Изменить класс Order - он должен формироваться
            //из класса Basket

        }
    }
}
