﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OrderDeliveryBuilder
{
    public class Basket
    {
        public Client Client { get; set; }
        public List<Product> Products { get; set; }
    }
}
