﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OrderDeliveryBuilder.Pattern.ConcreteBuilder
{
    class Manager
    {
        private Delivery delivery;
        private Manager() { }
        public Manager(Delivery delivery,
            string date,
            string address,
            string person
            )
        {
            this.delivery = delivery;
            this.delivery.Address = address;
            this.delivery.Date = date;
            this.delivery.Person = person;
           
        }
        public void addToDelivery(Order order)
        {
            this.delivery.add(order);
        }
        //как правило должен быть метод
        //возвращающий Построенный или настроенный объект
        public Delivery getCompletedDelivery()
        {
            return this.delivery;
        }
        //
    }
}
