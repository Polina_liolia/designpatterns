﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OrderDeliveryBuilder.Pattern.ConcreteBuilder
{
    public class Order
    {
        public string Caption;//название заказа
        public string Date;//дата заказа
        public List<string> Positions;//"состав" заказа
        public double Price;//стоимость 


        public Order(Basket basket)
        {
            Date = DateTime.Now.ToLongDateString();
            Price = 0;
            Positions = new List<string>();
            foreach(Product p in basket.Products)
            {
                Positions.Add(p.Name);
                Price += p.Price;
            }
        }
    }
}
