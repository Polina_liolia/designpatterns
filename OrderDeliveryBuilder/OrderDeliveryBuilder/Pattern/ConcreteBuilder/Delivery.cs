﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OrderDeliveryBuilder.Pattern.ConcreteBuilder
{
    class Delivery 
    {
        public string Address;//куда
        public string Date;//когда
        public string Person;//кому
        public double Cost;//стоимость доставки
        public Delivery()
        {
            Cost = 0.0;
        }
        private List<Order> Object = new List<Order>();//предмет доставки
        public void add(Order order)
        {
            Object.Add(order);
            Cost += order.Price;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Address: {Address}");
            sb.AppendLine($"Date: {Date}");
            sb.AppendLine($"Person: {Person}");
           
            sb.AppendLine("Orders to deliver:");
            int orderCounter = 1;
            foreach (Order order in Object)
            {
                sb.AppendLine($"{orderCounter}.{order.Caption}");
                sb.AppendLine($"Date: {order.Date}");
                sb.AppendLine("Positions:");
                int positionsCounter = 1;
                foreach (string product in order.Positions)
                { 
                    sb.AppendLine($"{orderCounter}.{positionsCounter++}.{product}");
                }
                sb.AppendLine($"Order total: {order.Price}");
                orderCounter++;
            }
            sb.AppendLine($"Total cost: {Cost}");
            return sb.ToString();
        }
    }
}
