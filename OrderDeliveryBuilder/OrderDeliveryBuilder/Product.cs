﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OrderDeliveryBuilder
{
    public class Product
    {
        public string Name { get; set; }
        public double Price { get; set; }

    }
}
