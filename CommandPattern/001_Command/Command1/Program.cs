﻿using System;
using System.Collections.Generic;

namespace PatternCommand
{
    class Program
    {
        static void Main()
        {
            Receiver receiver = new Receiver();
            Command command = new ConcreteCommand(receiver);
            Invoker invoker = new Invoker();

            invoker.StoreCommand(command);
            invoker.ExecuteCommand();
            //дальше можно создать коллекцию и вызвать в цикле
            List<Invoker> commands = new List<Invoker>();
            commands.Add(invoker);
            Command command2 = new ConcreteCommand(receiver);
            Invoker invoker2 = new Invoker();
            invoker2.StoreCommand(command2);
            commands.Add(invoker2);

            foreach (Invoker i in commands)
                i.ExecuteCommand();
        }
    }
}
