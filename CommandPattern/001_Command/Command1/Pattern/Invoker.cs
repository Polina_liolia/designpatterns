﻿using System;
using System.Collections.Generic;

namespace PatternCommand
{
    //classic
    class Invoker
    {
        Command command;

        public void StoreCommand(Command command)
        {
            this.command = command;
        }

        public void ExecuteCommand()
        {
            command.Execute();
        }
    }

    //upgreaded version
    class MyInvoker
    {
        List<Command> commands;

        public void StoreCommand(Command command)
        {
            this.commands.Add(command);
        }
        public void ExecuteCommand(int index)
        {
            commands[index].Execute();
        }

        public void ExecuteCommands()
        {
            for (int i = 0; i < commands.Count; i++)
                ExecuteCommand(i);
        }
    }
}
