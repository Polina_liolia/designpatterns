﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Command.Pattern.Commands
{
    class Sqrt : Command
    {
        public Sqrt(ArithmeticUnit unit)
        {
            this.unit = unit;
        }
        public override void Execute()
        {
            unit.Run('s');
        }

        public override void UnExecute()
        {
            unit.Run('^');
        }
    }
}
