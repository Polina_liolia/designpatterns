﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Command.Pattern.Commands
{
    class Pow : Command
    {
        public Pow(ArithmeticUnit unit)
        {
            this.unit = unit;
        }

        public override void Execute()
        {
            unit.Run('^');
        }

        public override void UnExecute()
        {
            unit.Run('s');
        }
    }
}
