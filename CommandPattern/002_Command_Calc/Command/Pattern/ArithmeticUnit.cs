using System;

namespace Command
{
    // "Receiver" (����������) - �������������� ���������� (��)
    class ArithmeticUnit
    {
        public double Register { get; private set; }

        public void Run(char operationCode, double operand = 0)
        {
            if (operand == 0) //default
                operand = Register;
            switch (operationCode)
            {
                case '+':
                    Register += operand;
                    break;
                case '-':
                    Register -= operand;
                    break;
                case '*':
                    Register *= operand;
                    break;
                case '/':
                    Register /= operand;
                    break;
                case '^':
                    Register = Math.Pow(operand, 2);
                    break;
                case 's':
                    Register = Math.Sqrt(operand);
                    break;
            }
        }
    }
}
