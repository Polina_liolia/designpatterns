using System;

namespace Command
{
    class Program
    {
        //�� �������������� ����������� �������� Sqrt
        static void Main()
        {
            Calculator calculator = new Calculator();
            double result = 0;

            result = calculator.Add(5);
            Console.WriteLine(result);

            result = calculator.Sub(3);
            Console.WriteLine(result);

            Console.WriteLine(new string('-', 3));

            result = calculator.Pow();
            Console.WriteLine(result);

            result = calculator.Pow();
            Console.WriteLine(result);

            result = calculator.Sqrt();
            Console.WriteLine(result);

            result = calculator.Undo(2);
            Console.WriteLine(result);

            result = calculator.Redo(1);
            Console.WriteLine(result);

            result = calculator.Add(1);
            Console.WriteLine(result);

            // Delay.
            Console.ReadKey();
        }
    }
}
