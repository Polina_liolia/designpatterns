﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Drawer
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Paint(object sender, PaintEventArgs e)
        {
            Graphics gr = Graphics.FromHwnd(this.Handle);
            DrawerSingleton drawer = DrawerSingleton.Instance;
            drawer.Graphics = gr;

            IShape shape1, shape2;
            ShapesFactory shapesFactory = new ShapesFactory(drawer);

            //Создание фабричного метода:
            //всегда возвращает интерфейс
            //приниает тип объекта, который необходимо создать (если метод не является узко специализированным)
            //создание метода перебора (switch)
            //делаем метод статическим

            shape1 = shapesFactory.Create("SQUARE");
            shape2 = shapesFactory.Create("RECTANGLE");

            ShapesHolder.Shapes.AddRange(new IShape[] { shape1, shape2 });


            //нужно перейти на использование GetType
            List<string> shapes = new List<string> { "RECTANGLE", "CIRCLE", "SQUARE" };
            drawer.DrawShapes(shapes);
            MathShapesCalculatorSingleton calculator = MathShapesCalculatorSingleton.Instance;
            calculator.CalculateMathShapes(shapes);
            
        }
    }
}
