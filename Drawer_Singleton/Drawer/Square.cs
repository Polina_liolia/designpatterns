﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Drawer
{
    //ответственность: описывает понятие квадрат
    //центр и ширина
    //ответственность - раскрывется через агрегацию или композицию
    public class Square : IShape
    {
        protected DrawerSingleton drawer; //этот объект нарушает принцип единственносй ответственности класса //АГРЕГАЦИЯ
        protected int width;
        protected int x;
        protected int y;
        public string TypeShape { get; set; }

        public Square(DrawerSingleton dr, int x, int y, int width)
        {
            this.drawer = dr;
            this.x = x;
            this.y = y;
            this.width = width;
            TypeShape = "SQUARE";
        }
       
        
        public void Draw() //класс Rectangle зависит от Graphics через свойство в  MyDrawer, которое его содержит
        {
            if (drawer.Graphics != null)
                drawer.Graphics.DrawRectangle(Pens.Red, (x - width/2), (y - width/2), width, width);
        }

        public double calcS()
        {
            return width * width;
        }

        public double calcP()
        {
            return width * 4;
        }
    }
}
