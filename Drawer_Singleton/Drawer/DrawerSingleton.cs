﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Drawer
{
    public sealed class DrawerSingleton
    {
        public Graphics Graphics { get; set; } //агрегация 

        private DrawerSingleton()
        {

        }

        #region Instance holder
        public static DrawerSingleton Instance
        {
            get
            {
                return InstanceHolder.instance;
            }
        }


        private class InstanceHolder
        {
            static InstanceHolder()
            {

            }
            internal static readonly DrawerSingleton instance = new DrawerSingleton();
        }
        #endregion


        public void DrawShapes(List<string> types)
        {
            foreach (IGraphicShape shape in ShapesHolder.Shapes)
                foreach (string type in types)
                    if (types.Contains(((IShape)shape).TypeShape))
                    {
                        shape.Draw();
                        break;
                    }
        }
    }
}
