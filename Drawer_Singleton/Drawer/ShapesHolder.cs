﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Drawer
{
    public static class ShapesHolder
    {
        public static List<IShape> Shapes { get; }
        static ShapesHolder()
        {
            Shapes = new List<IShape>();
        }
    }
}
