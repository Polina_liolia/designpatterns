﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Drawer
{
    public sealed class MathShapesCalculatorSingleton
    {        
        public static MathShapesCalculatorSingleton Instance
        {
            get
            {
                return InstanceHolder.instance;
            }
        }

        private MathShapesCalculatorSingleton()
        {
        }
        #region Instance holder
        private class InstanceHolder
        {
            static InstanceHolder() {}
            internal static readonly MathShapesCalculatorSingleton instance = new MathShapesCalculatorSingleton();
        }
        #endregion
 

        public void CalculateMathShapes(List<string> types)
        {
            foreach(IMathShape shape in ShapesHolder.Shapes)
            {
                foreach(string type in types)
                {
                    if(((IShape)(shape)).TypeShape == type)
                    {
                        double s = shape.calcS();
                        double p = shape.calcP();
                        MessageBox.Show($"Square: {s}, Perimeter: {p}", $"{((IShape)(shape)).TypeShape}");
                    }
                }
            }
        }
    }
}
