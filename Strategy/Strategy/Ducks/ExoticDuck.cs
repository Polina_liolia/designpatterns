﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Strategy.Fly;
using Strategy.Quack;

namespace Strategy.Ducks
{
    public class ExoticDuck : Base_duck
    {
        public override void display()
        {
            Console.WriteLine("Hi! I'm an Exotic duck!");
        }

        public ExoticDuck()
        {
            quackability = new StrangeQuack();
        }

        
    }
}
