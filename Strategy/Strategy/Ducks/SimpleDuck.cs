﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Strategy.Fly;
using Strategy.Quack;

namespace Strategy.Ducks
{
    public class SimpleDuck : Base_duck
    {
        public override void display()
        {
            Console.WriteLine("Hi! I'm a Simple duck!");
        }

        public SimpleDuck()
        {

        }
    }
}
