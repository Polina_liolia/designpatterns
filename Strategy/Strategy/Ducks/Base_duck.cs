﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Strategy.Fly;
using Strategy.Quack;

namespace Strategy.Ducks
{
    public abstract class Base_duck 
    {
        protected IFlyable flyabitity;
        protected IQuackable quackability;

        public Base_duck()
        {
            flyabitity = new SimpleFly();
            quackability = new SimpleQuack();
        }

        public abstract void display();

        public void swim()
        {
            Console.WriteLine("I'm swimming!");
        }
        public void fly()
        {
            flyabitity.fly();
        }
        public void quack()
        {
            quackability.quack();
        }

    }
}
