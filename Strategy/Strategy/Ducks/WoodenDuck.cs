﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Strategy.Fly;
using Strategy.Quack;

namespace Strategy.Ducks
{
    public class WoodenDuck : Base_duck
    {
        public override void display()
        {
            Console.WriteLine("Hi! I'm a Wooden duck!"); 
        }
        public WoodenDuck()
        {
            flyabitity = new NoFly();
            quackability = new NoQuack();
        }
    }
}
