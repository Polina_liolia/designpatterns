﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy.Quack
{
    public class StrangeQuack : IQuackable
    {
        public void quack()
        {
            Console.WriteLine("QuuuQua! QuuuQua!");
        }
    }
}
