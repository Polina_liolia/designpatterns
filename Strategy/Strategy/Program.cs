﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Strategy.Fly;
using Strategy.Quack;
using Strategy.Ducks;

namespace Strategy
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Base_duck> ducks = new List<Base_duck>();
            ducks.Add(new ExoticDuck());
            ducks.Add(new RubberDuck());
            ducks.Add(new SimpleDuck());
            ducks.Add(new WoodenDuck());

            foreach(Base_duck d in ducks)
            {
                d.display();
                d.swim();
                d.fly();
                d.quack();
                Console.WriteLine();
            }
        }
    }
}
