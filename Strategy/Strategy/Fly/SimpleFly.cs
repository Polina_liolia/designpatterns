﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy.Fly
{
    public class SimpleFly : IFlyable
    {
        public void fly()
        {
            Console.WriteLine("I'm flying!");
        }
    }
}
