﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Observer.Widgets;

namespace Observer.News
{
   

    public class NewsAggregator : ISubject
    {
        private static Random random;
        private List<IObserver> observers;
       

        public NewsAggregator()
        {
            random = new Random();
            observers = new List<IObserver>();
        }

        public string getTwitterNews()
        {
            // эмуляция запроса на сервер для получения новости
            List<string> news = new List<string>()
            {
                "Twitter news 1",
                "Twitter news 2",
                "Twitter news 3"
            };
            return news[random.Next(3)];
        }

        public string getLentaNews()
        {
            // эмуляция запроса на сервер для получения новости
            List<string> news = new List<string>()
            {
                "Lenta news 1",
                "Lenta news 2",
                "Lenta news 3"
            };
            return news[random.Next(3)];
        }

        public string getTVNews()
        {
            // эмуляция запроса на сервер для получения новости
            List<string> news = new List<string>()
            {
                "TV news 1",
                "TV news 2",
                "TV news 3"
            };
            return news[random.Next(3)];
        }

        public void NewNewsAvailable()
        {
            NotifyObservers();
        }

        #region ISubject
        public void RegisterObserver(IObserver observer)
        {
            observers.Add(observer);
        }

        public void RemoveObserver(IObserver observer)
        {
            observers.Remove(observer);
        }

        public void NotifyObservers()
        {
            string twitter_news = getTwitterNews();
            string tv_news = getTVNews();
            string lenta_news = getLentaNews();

            foreach (IObserver widget in observers)
                widget.Update(twitter_news, lenta_news, tv_news); 
        }
        #endregion
    }

    
}
