﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Observer.News;
using Observer.Widgets;

namespace Observer
{
    class Program
    {
        static void Main(string[] args)
        {
            NewsAggregator newsAggregator = new NewsAggregator();
            TwitterWidget twitterWidget = new TwitterWidget(newsAggregator);
            LentaWidget lentaWidget = new LentaWidget(newsAggregator);
            TVWidget tvWidget = new TVWidget(newsAggregator);
            
            newsAggregator.NewNewsAvailable();
            twitterWidget.RemoveFromSubject();
            newsAggregator.NewNewsAvailable();
        }
    }
}
