﻿using Observer.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer.Widgets
{
    class TwitterWidget : IObserver, IWidget
    {
        private string twitter;
        private ISubject subject;

        public TwitterWidget(ISubject subject)
        {
            this.subject = subject; 
            subject.RegisterObserver(this);
        }

        public void Update(string twitter, string lenta, string tv)
        {
            this.twitter = twitter;
            Display();
        }

        public void Display()
        {
            Console.WriteLine("Twitter: {0}", twitter);
        }

        public void RemoveFromSubject()
        {
            subject.RemoveObserver(this);
        }
    }
}
