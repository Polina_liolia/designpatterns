﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Observer.News;

namespace Observer.Widgets
{
    public class LentaWidget : IObserver, IWidget
    {
        private string lenta;
        private ISubject subject;

        public LentaWidget(ISubject subject)
        {
            this.subject = subject;
            this.subject.RegisterObserver(this);
        }

        public void Update(string twitter, string lenta, string tv)
        {
            this.lenta = lenta;
            Display();
        }

        public void Display()
        {
            Console.WriteLine("Lenta: {0}", lenta);
        }

        public void RemoveFromSubject()
        {
            subject.RemoveObserver(this);
        }
    }
}
