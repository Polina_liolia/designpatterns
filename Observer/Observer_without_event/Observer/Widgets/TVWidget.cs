﻿using Observer.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer.Widgets
{
    public class TVWidget : IObserver, IWidget
    {
        private string tv;
        private ISubject subject;

        public TVWidget(ISubject subject)
        {
            this.subject = subject;
            this.subject.RegisterObserver(this);
        }

        public void Update(string twitter, string lenta, string tv)
        {
            this.tv = tv;
            Display();
        }

        public void Display()
        {
            Console.WriteLine("TV: {0}", tv);
        }
        
        public void RemoveFromSubject()
        {
            subject.RemoveObserver(this);
        }
    }
}
