﻿using Observer.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer.Widgets
{
    public class TVWidget : IWidget
    {
        private string tv;

        public void Update(object sender, NewsEventsArgs e)
        {
            this.tv = e.TV;
            Display();
        }

        public void Display()
        {
            Console.WriteLine("TV: {0}", tv);
        }
    }
}
