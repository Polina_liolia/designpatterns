﻿using Observer.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer.Widgets
{
    class TwitterWidget : IWidget
    {
        private string twitter;
       
        public void Update(object sender, NewsEventsArgs e)
        {
            this.twitter = e.Twitter;
            Display();
        }

        public void Display()
        {
            Console.WriteLine("Twitter: {0}", twitter);
        }

    }
}
