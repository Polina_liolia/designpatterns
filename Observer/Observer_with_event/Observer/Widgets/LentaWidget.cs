﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Observer.News;

namespace Observer.Widgets
{
    public class LentaWidget : IWidget
    {
        private string lenta;

        public void Update(object sender, NewsEventsArgs e)
        {
            this.lenta = e.Lenta;
            Display();
        }

        public void Display()
        {
            Console.WriteLine("Lenta: {0}", lenta);
        }
    }
}
