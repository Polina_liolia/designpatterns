﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Observer.Widgets;

namespace Observer.News
{
   

    public class NewsAggregator
    {
        private static Random random;
      
        public delegate void NewsChangedEventHandler(object sender, NewsEventsArgs e);
        public event NewsChangedEventHandler NewsChanged;
        public NewsAggregator()
        {
            random = new Random();
        }

        public string getTwitterNews()
        {
            // эмуляция запроса на сервер для получения новости
            List<string> news = new List<string>()
            {
                "Twitter news 1",
                "Twitter news 2",
                "Twitter news 3"
            };
            return news[random.Next(3)];
        }

        public string getLentaNews()
        {
            // эмуляция запроса на сервер для получения новости
            List<string> news = new List<string>()
            {
                "Lenta news 1",
                "Lenta news 2",
                "Lenta news 3"
            };
            return news[random.Next(3)];
        }

        public string getTVNews()
        {
            // эмуляция запроса на сервер для получения новости
            List<string> news = new List<string>()
            {
                "TV news 1",
                "TV news 2",
                "TV news 3"
            };
            return news[random.Next(3)];
        }

        public void NewNewsAvailable()
        {
            string twitter_news = getTwitterNews();
            string tv_news = getTVNews();
            string lenta_news = getLentaNews();
            if (NewsChanged != null)
                NewsChanged(this, new NewsEventsArgs(lenta_news, twitter_news, tv_news));
        }
    }

    public class NewsEventsArgs
    {
        public string Lenta { get; private set; }
        public string Twitter { get; private set; }
        public string TV { get; private set; }
        public NewsEventsArgs(string lenta, string twitter, string tv)
        {
            Lenta = lenta;
            Twitter = twitter;
            TV = tv;
        }
     }
}
