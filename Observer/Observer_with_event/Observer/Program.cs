﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Observer.News;
using Observer.Widgets;

namespace Observer
{
    class Program
    {
        static void Main(string[] args)
        {
            NewsAggregator newsAggregator = new NewsAggregator();
            TwitterWidget twitterWidget = new TwitterWidget();
            LentaWidget lentaWidget = new LentaWidget();
            TVWidget tvWidget = new TVWidget();

            newsAggregator.NewsChanged += twitterWidget.Update;
            newsAggregator.NewsChanged += lentaWidget.Update;
            newsAggregator.NewsChanged += tvWidget.Update;

            newsAggregator.NewNewsAvailable();
            
            newsAggregator.NewNewsAvailable();
        }
    }
}
