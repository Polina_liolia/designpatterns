﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomateTester_StatePattern
{
    class Program
    {
        static void Main(string[] args)
        {
            FiniteStateMashine stateMashine = FiniteStateMashine.Create();
            stateMashine.DefineState("123");

            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }
    }
}
