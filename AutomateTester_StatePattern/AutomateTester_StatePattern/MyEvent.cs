﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomateTester_StatePattern
{
    public enum MyEvents
    {
        SEND_ERROR,
        SEND_CHAR,
        SEND_DIGIT,
        SEND_DIV
    }
}
