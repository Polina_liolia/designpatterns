﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomateTester_StatePattern.States
{
    public class IdentState : State
    {
        protected override bool ChangeState(InputString str, MyEvents myEvent)
        {
            bool finalState = false;
            switch (myEvent)
            {
                case MyEvents.SEND_CHAR:
                    str.State = new IdentState(); break;
                case MyEvents.SEND_DIGIT:
                    str.State = new IdentState(); break;
                case MyEvents.SEND_DIV:
                    {
                        str.State = new StringState();
                        finalState = true; 
                        break;
                    }
            }
            return finalState;
        }
        public override string ToString()
        {
            return "Ident";
        }
    }
}
