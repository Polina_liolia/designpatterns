﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomateTester_StatePattern.States
{
    public class DigitState : State
    {
        protected override bool ChangeState(InputString str, MyEvents myEvent)
        {
            bool finalState = false;
            switch (myEvent)
            {
                case MyEvents.SEND_CHAR:
                    str.State = new StringState(); break;
                case MyEvents.SEND_DIGIT:
                    str.State = new DigitState(); break;
                case MyEvents.SEND_DIV:
                    str.State = new DoubleState(); break;
            }
            return finalState;
        }

        public override string ToString()
        {
            return "Digit";
        }
    }
}
