﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomateTester_StatePattern.States
{
    public abstract class State
    {
        protected abstract bool ChangeState(InputString str, MyEvents myEvent);

        public virtual bool HandleEvent(InputString str, MyEvents myEvent)
        {
            return ChangeState(str, myEvent);
        }
    }
}
