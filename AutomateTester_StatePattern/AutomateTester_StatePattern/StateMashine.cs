﻿using AutomateTester_StatePattern.States;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomateTester_StatePattern
{
    public class FiniteStateMashine
    {
        private FiniteStateMashine()
        {

        }

        public static FiniteStateMashine Create()
        {
            return new FiniteStateMashine();
        }

        private MyEvents GenerateEvent(char ch)
        {
            int result = -1;
            switch (ch)
            {
                case '0': result = 0; break;
                case '1': result = 1; break;
                case '2': result = 2; break;
                case '3': result = 3; break;
                case '4': result = 4; break;
                case '5': result = 5; break;
                case '6': result = 6; break;
                case '7': result = 7; break;
                case '8': result = 8; break;
                case '9': result = 9; break;
            }
            return result > 0 ? MyEvents.SEND_DIGIT :
                (ch == '.' || ch == ',') ? MyEvents.SEND_DIV :
                MyEvents.SEND_CHAR;
        }

        public void DefineState(string str)
        {
            InputString inputStr = new InputString(str);
            foreach(char ch in inputStr.InputStr)
            {
                MyEvents currentEvent = GenerateEvent(ch);
                bool stateDefined = inputStr.FindOut(currentEvent);
                if (stateDefined)
                    break;
            }
            Console.WriteLine(inputStr.State);
        }
    }
}
