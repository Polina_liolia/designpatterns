﻿using AutomateTester_StatePattern.States;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomateTester_StatePattern
{
    public class InputString
    {
        public string InputStr { get; set; }
        public State State { get; set; }

        public InputString(string str)
        {
            InputStr = str;
            State = new EmptyState();
        }
        public bool FindOut(MyEvents myEvent)
        {
            return State.HandleEvent(this, myEvent);
        }
    }
}
