﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//создать автомат на основе switch, который определяет,
//какая строка пред нами: 
// пустая
// содержит корректную запись целого числа
// содержит набор символов
// содержит корректную запись дробного числа
// содержит корректный идентификатор на языке С
namespace AutomateTester
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = "123";
            int result = start_state_finite_mashine(str);
            string result_text = result == EMPTY ? "пустая" :
                result == STRING ? "содержит набор символов" :
                result == DIGIT ? "содержит корректную запись целого числа" :
                result == DOUBLE ? "содержит корректную запись дробного числа" :
                result == IDENT ? "содержит корректный идентификатор на языке С" : "ERROR";
            Console.WriteLine($"{str} is {result_text}");
        }

        #region Events
        const int SEND_ERROR = -1;
        const int SEND_CHAR = 1;
        const int SEND_DIGIT = 2;
        const int SEND_DIV = 3;
        #endregion

        static int generate_event(char ch)
        {
            int result = SEND_ERROR;
            switch (ch)
            {
                case '0': result = 0; break;
                case '1': result = 1; break;
                case '2': result = 2; break;
                case '3': result = 3; break;
                case '4': result = 4; break;
                case '5': result = 5; break;
                case '6': result = 6; break;
                case '7': result = 7; break;
                case '8': result = 8; break;
                case '9': result = 9; break;
            }
            return result > 0 ? SEND_DIGIT :
                (ch == '.' || ch == ',') ? SEND_DIV :
                SEND_CHAR;
        }

        #region States
        const int EMPTY = 0;    // пустая
        const int STRING = 1;   // содержит набор символов
        const int DIGIT = 2;    // содержит корректную запись целого числа
        const int DOUBLE = 3;   // содержит корректную запись дробного числа
        const int IDENT = 4;    // содержит корректный идентификатор на языке С
        #endregion

        static int start_state_finite_mashine(string s)
        {
            int END_LOOP = s.Length;
            int STATE = EMPTY;
            int i = 0;
            do
            {
                //т.к. event - ключевое слово
                int EVENT = generate_event(s[i]);
                switch (STATE)
                {
                    case EMPTY:
                        {
                            switch (EVENT)
                            {
                                case SEND_CHAR:
                                    STATE = STRING; break;
                                case SEND_DIGIT:
                                    STATE = DIGIT; break;
                                case SEND_DIV:
                                    STATE = DOUBLE; break;
                            }
                        }
                        break;
                    case STRING:
                        {
                            switch (EVENT)
                            {
                                case SEND_CHAR:
                                    STATE = STRING; break;
                                case SEND_DIGIT:
                                    {
                                        STATE = IDENT;
                                        END_LOOP = i; //brake do...while loop
                                    }
                                    break;
                                case SEND_DIV:
                                    STATE = STRING; break;
                            }
                            break;
                        }
                    case DIGIT:
                        {
                            switch (EVENT)
                            {
                                case SEND_CHAR:
                                    STATE = STRING; break;
                                case SEND_DIGIT:
                                    STATE = DIGIT; break;
                                case SEND_DIV:
                                    STATE = DOUBLE; break;
                            }
                            break;
                        }
                    case DOUBLE:
                        {
                            switch (EVENT)
                            {
                                case SEND_CHAR:
                                    {
                                        STATE = STRING;
                                        END_LOOP = i; //brake do...while loop
                                        break;
                                    }
                                case SEND_DIGIT:
                                    STATE = DOUBLE; break;
                                case SEND_DIV:
                                    {
                                        STATE = STRING;
                                        END_LOOP = i; //brake do...while loop
                                        break;
                                    }
                            }
                            break;
                        }
                    case IDENT:
                        {
                            switch (EVENT)
                            {
                                case SEND_CHAR:
                                    STATE = IDENT; break;
                                case SEND_DIGIT:
                                    STATE = IDENT; break;
                                case SEND_DIV:
                                    {
                                        STATE = STRING;
                                        END_LOOP = i; //brake do...while loop
                                        break;
                                    }
                            }
                            break;
                        }
                }
                i++;
            } while (i < END_LOOP);
            return STATE;
        }
    }
}
