﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SingletonTest
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Тест версии из учебника
            TestSingleton obg1 = TestSingleton.getInstance("obj1"); //constructor call, name set
            TestSingleton obg2 = TestSingleton.getInstance("obj2"); //no constructor call
            TestSingleton obg3 = TestSingleton.getInstance("obj3"); //no constructor call

            Console.WriteLine(obg1.Name);
            Console.WriteLine(obg2.Name);
            Console.WriteLine(obg3.Name);
            #endregion

            #region Тест версии решения проблемы до 4.0 .NET
            Singleton singletonInstance = Singleton.Instance;
            #endregion

            #region Тест версии от 4.0 .NET

            #endregion

            Console.WriteLine("Press any key...");
            Console.ReadKey();

        }
    }

    //singleton используется, если сам процесс создания объекта ресурсоъемкий (сложный процесс конструирования объекта)
    //но минус в том, что такой статически созданный экземпляр не будет удален сборщиком мусора

    #region "Версия из учебника"
    public class TestSingleton
    {
        private static TestSingleton instance;
        public string Name { get; private set; }

        #region Constructors
        private TestSingleton() { }
        protected TestSingleton(string name)
        {
            Name = name;
        }
        #endregion

        public static TestSingleton getInstance(string name)
        {
            if (instance == null)
                instance = new TestSingleton(name);
            return instance;
        }
    }
    #endregion

    #region Решение проблемы до 4.0 .NET - на использовании вложенных классов
    //основываясь на ECMA 335 спецификации CLI раздел 8.9.5
    public sealed class Singleton
    {
        private Singleton() { }
        public static Singleton Instance
        {
            get
            {
                return InstanceHolder.instance;
            }
        }
        private class InstanceHolder
        {
            internal static readonly Singleton instance = new Singleton();
            static InstanceHolder()
            {

            }
        }
    }
    #endregion

    #region Решение проблемы от 4.0 .NET на основе Lazy классов
    public sealed class Singleton2
    {
        private static readonly Lazy<Singleton2> instance = new Lazy<Singleton2>(() => new Singleton2());
        private Singleton2() { }

        public static Singleton2 Instance
        {
            get
            {
                return Singleton2.instance.Value;
            }
        }
    }
    #endregion
}
