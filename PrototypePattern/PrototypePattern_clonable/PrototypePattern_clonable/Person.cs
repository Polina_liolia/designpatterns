﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrototypePattern_clonable
{
    public class Person : IEquatable<Person>, ICloneable
    {
        private string uniqueSsn;
        public string SSN
        {
            get { return this.uniqueSsn; }
            set { uniqueSsn = value; }
        }
        private string lName;
        public string LastName
        {
            get { return this.lName; }
            set
            {
                if (String.IsNullOrEmpty(value))
                    throw new ArgumentException("The last name cannot be null or empty.");
                else
                    this.lName = value;
            }
        }
        #region additional Fields
        public int Age;
        public string Name;
        public IdInfo IdInfo;
        #endregion
        public Person(string lastName, string ssn)
        {
            this.SSN = ssn;
            this.LastName = lastName;
        }
        protected Person() { }
        #region Equals Implementation
        public bool Equals(Person other)//реализация интерфейса IEquatable
        {
            if (other == null)
                return false;
            if (this.uniqueSsn == other.uniqueSsn)
                return true;
            else
                return false;
        }
        public override bool Equals(Object obj)//реализация System.Object
        {
            if (obj == null)
                return false;
            Person personObj = obj as Person;
            if (personObj == null)
                return false;
            else
                return Equals(personObj);//вызов реализации для интерфейса-строгий тип
        }
        public override int GetHashCode()//из System.Object для Dictionary
        {
            return this.SSN.GetHashCode();
            //должа основываться на полях класса, которые задействованы в Equals
            //из System.Object
        }
        //операторы сравнения "==" и "!=" основываются на Equals из System.Object
        public static bool operator ==(Person person1, Person person2)
        {
            if (((object)person1) == null || ((object)person2) == null)
                return Object.Equals(person1, person2);

            return person1.Equals(person2);
        }
        public static bool operator !=(Person person1, Person person2)
        {
            if (((object)person1) == null || ((object)person2) == null)
                return !Object.Equals(person1, person2);

            return !(person1.Equals(person2));
        }
        #endregion
        //
        public Person ShallowCopy()//аналог String clone = (string)str.Clone();//удвоение ссылок:
        {
            return (Person)this.MemberwiseClone();//не зарагивает ссылочные типы
            //внутри нашего объекта - поддержка клонирования .Net-ом
        }
        public Person DeepCopy()//правильная реализация интерфейса ICloneable
        {
            Person other = (Person)this.MemberwiseClone();
            //все "свои" ссылочные типы заполняем сами
            other.IdInfo = new IdInfo(IdInfo.IdNumber);
            other.Name = String.Copy(Name);
            return other;
        }
        //
        public object Clone()
        {
            return DeepCopy();
        }
    }
}
