﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrototypePattern_clonable
{
    public class MyPerson : ICloneable
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public Address PersonAddress { get; set; }

        public object Clone()
        {
            MyPerson newPerson = (MyPerson)this.MemberwiseClone();
            newPerson.PersonAddress = (Address)this.PersonAddress.Clone();
            return newPerson;
        }
    }
}
