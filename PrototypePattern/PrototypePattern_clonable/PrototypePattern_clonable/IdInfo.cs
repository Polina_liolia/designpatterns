﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrototypePattern_clonable
{
    public class IdInfo
    {
        public int IdNumber;

        public IdInfo(int IdNumber)
        {
            this.IdNumber = IdNumber;
        }
    }
}
