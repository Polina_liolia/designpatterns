﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Drawer
{
    public interface IShape
    {
        string TypeShape { get; set; }
        void Draw();
    }
}
