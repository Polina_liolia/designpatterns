﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Drawer
{
    public class MyDrawer
    {
        private List<IShape> shapes; //композиция
        public Graphics Graphics { get; private set; } //агрегация
        private List<string> registerdShapes = new List<string> { "SQUARE", "RECTANGLE" }; //композиция 
        public MyDrawer(Graphics gr)
        {
            Graphics = gr;
            IShape shape1, shape2;
            SquareFactory squareFactory = new SquareFactory(this);
            RectangleFactory rectangleFactory = new RectangleFactory(this);

            //Создание фабричного метода:
            //всегда возвращает интерфейс
            //приниает тип объекта, который необходимо создать (если метод не является узко специализированным)
            //создание метода перебора (switch)
            //делаем метод статическим (или используем фабрику)

            shape1 = squareFactory.Create();
            shape2 = rectangleFactory.Create();

            shapes = new List<IShape>();
            shapes.AddRange(new IShape[] { shape1, shape2 });

        }

        

        public void DrawShapes(List<string> types)
        {
            foreach (IShape shape in shapes)
                foreach (string type in types)
                    if (types.Contains(shape.TypeShape))
                    {
                        shape.Draw();
                        break;
                    }
        }
    }
}
