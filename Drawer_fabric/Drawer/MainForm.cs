﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Drawer
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Paint(object sender, PaintEventArgs e)
        {
            Graphics gr = Graphics.FromHwnd(this.Handle);
            MyDrawer drawer = new MyDrawer(gr);
            //нужно перейти на использование GetType
            List<string> shapes = new List<string> { "RECTANGLE", "CIRCLE", "SQUARE" };
            drawer.DrawShapes(shapes);
        }
    }
}
