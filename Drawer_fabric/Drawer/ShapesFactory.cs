﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Drawer
{

    public class SquareFactory : DefaultShapesFactory
    {
        public SquareFactory(MyDrawer drawer) : base(drawer)
        {
        }

        public IShape Create()
        {
            return Create("SQUARE");
        }
    }

    public class RectangleFactory : DefaultShapesFactory
    {
        public RectangleFactory(MyDrawer drawer) : base(drawer)
        {
        }

        public IShape Create()
        {
            return Create("RECTANGLE");
        }
    }


    public abstract class DefaultShapesFactory : IShapesFactory
    {
        MyDrawer drawer;
        public DefaultShapesFactory(MyDrawer drawer)
        {
            this.drawer = drawer;
        }

        public IShape Create()
        {
            return Create(string.Empty);

        }
        protected IShape Create(string type)
        {
            int x1 = 100, y1 = 200, w = 25, h = 50;
            int x2 = x1 + w;
            int y2 = y1;

            switch (type)
            {
                case "SQUARE": return new Square(drawer, x1, y1, w);
                case "RECTANGLE": return new Rectangle(drawer, x2, y2, w, h);
                default: return new Square(drawer, x1, y1, w);
            }

        }
    }
}
