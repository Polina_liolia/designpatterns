﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StrategyPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            //Strategy currentSort = new BubbleSortStrategy();
            //Strategy currentSort = new InsertSortStrategy();
            //Strategy currentSort = new SelectionSortStrategy();
            //Strategy currentSort = new DefaultSortStrategy();
            
            var context = Context.BubbleSort();
            //var context = Context.InsertSort();
            //var context = Context.SelectionSort();
            //var context = Context.Sort(); //for DefaultSortStrategy
            context.DoSortAction();
            context.printArray();

            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }
    }
}
