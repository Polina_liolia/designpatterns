﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StrategyPattern
{
    public class Context
    {
        Strategy strategy;
        int[] array = { 3, 5, 1, 2, 4 };
        private Context(Strategy concreteStrategy)
        {
            this.strategy = concreteStrategy;
        }
        public void DoSortAction()
        {
            strategy.Sort(ref array);
        }
        public void printArray()
        {
            foreach (int i in this.array)
            {
                Console.Write($"{i}, ");
            }
            Console.WriteLine();
        }

        public static Context BubbleSort()
        {
            return new Context(new BubbleSortStrategy());
        }
        public static Context InsertSort()
        {
            return new Context(new InsertSortStrategy());
        }
        public static Context SelectionSort()
        {
            return new Context(new SelectionSortStrategy());
        }
        public static Context Sort() //for DefaultSortStrategy
        {
            return new Context(new DefaultSortStrategy());
        }
    }
}
