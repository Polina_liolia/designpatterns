﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StrategyPattern
{
    public class SelectionSortStrategy : Strategy
    {
        public override void Sort(ref int[] array)
        {
            Console.WriteLine("Selection sort:");
            
            for (int i = 0; i < array.Length; i++)
            {
                int min_index = i;
                for (int j = i; j < array.Length; j++)
                {
                    if (array[min_index] > array[j])
                    {
                        min_index = j;
                    }
                }
                int temp = array[i];
                array[i] = array[min_index];
                array[min_index] = temp;
            }
        }
    }
}
