﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StrategyPattern
{
    public class BubbleSortStrategy : Strategy
    {
        public override void Sort(ref int[] array)
        {
            Console.WriteLine("Bubble sort:");
            for (int i = 0; i < array.Length - 1; i++)
            {
                for(int j = 1; j < array.Length; j++)
                {
                    if (array[j] < array[j-1])
                    {
                        int tmp = array[j];
                        array[j] = array[j - 1];
                        array[j - 1] = tmp;
                    }
                }
            }
        }
    }
}
