﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StrategyPattern
{
    public class DefaultSortStrategy : Strategy
    {
        public override void Sort(ref int[] array)
        {
            Console.WriteLine("Default: Array.Sort():");
            Array.Sort<int>(array);
        }
    }
}
