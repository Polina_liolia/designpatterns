﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StrategyPattern
{
    public abstract class Strategy
    {
        public abstract void Sort(ref int[] array);
    }
}
