﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StateMotivation
{
    public abstract class State
    {
        public virtual void HandleMark(Father father, Mark mark)
        {
            ChangeState(father, mark);
        }

        //правило смены состояния = реализация таблицы переходов
        protected abstract void ChangeState(Father father, Mark mark);
        protected abstract void OutAction();
    }
}
