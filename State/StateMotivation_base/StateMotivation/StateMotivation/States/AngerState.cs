﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StateMotivation.States
{
    public class AngerState : State
    {
        protected override void ChangeState(Father father, Mark mark)
        {
            switch (mark)
            {
                case Mark.Two:
                    {
                        father.State = new PityState(); // S1
                        break;
                    }
                case Mark.Five:
                    {
                        father.State = new JoyState();  // S3
                        break;
                    }
            }
        }
    }
}
