﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StateMotivation.States
{
    // Нейтральное состояние (S0)
    public class NeutralState : State
    {
        internal NeutralState()
        {
            Console.WriteLine("Отец в нейтральном состоянии:");
            OutAction();
        }

        protected override void ChangeState(Father father, Mark mark)
        {
            switch (mark)
            {
                case Mark.Two:
                    {
                        father.State = new PityState(); // S1
                        break;
                    }
                case Mark.Five:
                    {
                        father.State = new JoyState();  // S3
                        break;
                    }
            }
        }

        protected override void OutAction()
        {
            
        }

        private void Hope()
        {
            Console.WriteLine("Hope");
        }
       
    }
}
