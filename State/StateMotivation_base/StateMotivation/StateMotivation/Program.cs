﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StateMotivation
{
    class Program
    {
        static void Main(string[] args)
        {
            Father father = new Father();
            while(true)
            {
                Console.WriteLine("введите оценку сына (2 или 5):");
                //тесты:
                //вход 2,2,2 - выход y0(ремень)
                //вход 2,2,5,2 - выход y2(успокаивать)
                Mark mark = (Mark)Enum.Parse(typeof(Mark), Console.ReadLine());

                if (mark == Mark.Two || mark == Mark.Five)
                    father.FindOut(mark);
                else
                    Console.WriteLine("Нет таки оценок!");
            }
        }
    }
}
