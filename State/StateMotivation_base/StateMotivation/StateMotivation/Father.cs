﻿using StateMotivation.States;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StateMotivation
{
    // Context
    //реализует Context из паттерна State
    //см.диаграмму и описание на странице 1-2 State.pdf
    public class Father
    {
        internal State State { get; set; }

        public Father()
        {
            State = new NeutralState();
        }

        public void FindOut(Mark mark)
        {
            State.HandleMark(this, mark);
        }
    }
}
