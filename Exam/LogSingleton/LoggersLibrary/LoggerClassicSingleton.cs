﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoggersLibrary
{
    public class LoggerClassicSingleton : IDisposable
    {
        private List<string> logsNames;
        private StreamWriter sw;
        private bool use_C;
        private bool use_D;
        private bool use_F;
        private bool use_E;

        #region Constructor
        private LoggerClassicSingleton()
        {
            logsNames = new List<string>();
            logsNames.Add(@"c:\mylog.txt");
            logsNames.Add(@"d:\mylog.txt");
            logsNames.Add(@"e:\mylog.txt");
            logsNames.Add(@"f:\mylog.txt");
        }
        #endregion

        #region Singleton classic pattern
        private static LoggerClassicSingleton loggerInstance;
        public static LoggerClassicSingleton GetLogger()
        {
            if (loggerInstance == null)
            {
                loggerInstance =  new LoggerClassicSingleton();
            }
            return loggerInstance;
        }
        #endregion

        #region Public properties
        public bool Use_C
        {
            get
            {
                return use_C;
            }

            set
            {
                use_C = value;
                if (value)
                {
                    use_D = !value;
                    use_E = !value;
                    use_F = !value;
                }
            }
        }

        public bool Use_D
        {
            get
            {
                return use_D;
            }

            set
            {
                use_D = value;
                if (value)
                {
                    use_C = !value;
                    use_E = !value;
                    use_F = !value;
                }
            }
        }

        public bool Use_F
        {
            get
            {
                return use_F;
            }

            set
            {
                use_F = value;
                if (value)
                {
                    use_C = !value;
                    use_D = !value;
                    use_E = !value;
                }
            }
        }

        public bool Use_E
        {
            get
            {
                return use_E;
            }

            set
            {
                use_E = value;
                if (value)
                {
                    use_C = !value;
                    use_D = !value;
                    use_F = !value;
                }
            }
        }
        #endregion

        #region Indexators
        public string this[int index]
        {
            get { return logsNames[index]; }
        }

        public string this[string value]
        {
            get
            {
                if (value.Equals("C"))
                    return logsNames[0];
                if (value.Equals("D"))
                    return logsNames[1];
                if (value.Equals("E"))
                    return logsNames[2];
                if (value.Equals("F"))
                    return logsNames[3];
                else
                    throw new ArgumentOutOfRangeException("Such value was not found");
            }
        }
        #endregion

        #region WriteProtocol overloads
        //uses current log
        public void WriteProtocol(string action, string whoCalled, string description)
        {
            string currentLogName = use_C ? this["C"] :
                use_D ? this["D"] :
                use_E ? this["E"] : this["F"];
            try
            {
                sw = new StreamWriter(currentLogName, true, System.Text.Encoding.Default);
                sw.WriteLine(DateTime.Now.ToString());
                sw.WriteLine(action);
                sw.WriteLine(whoCalled);
                sw.WriteLine(description);
                sw.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }
        //uses directly pointed log
        public void WriteProtocol(string logName, string action, string whoCalled, string description)
        {
            try
            {
                StreamWriter sw = new StreamWriter(logName, true, System.Text.Encoding.Default);
                sw.WriteLine(DateTime.Now.ToString());
                sw.WriteLine(action);
                sw.WriteLine(whoCalled);
                sw.WriteLine(description);
                sw.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        #endregion

        #region IDisposable
        public void Dispose()//for manual call
        {
            sw.Dispose();
        }
        void IDisposable.Dispose() //auto-called
        {
            sw.Dispose();
        }

        #endregion
    }
}
