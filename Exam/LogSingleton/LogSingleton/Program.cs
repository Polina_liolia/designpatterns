﻿using LoggersLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogSingleton
{
    class Program
    {
        static void Main(string[] args)
        {
            using (LoggerClassicSingleton loggerClassicSingleton = LoggerClassicSingleton.GetLogger())
            {
                loggerClassicSingleton.Use_D = true;
                loggerClassicSingleton.WriteProtocol("logger created", "LoggerClassicSingleton", "");
            }
            using (LoggerNestedClassSingleton loggerNestedClassSingleton = LoggerNestedClassSingleton.Instance)
            {
                loggerNestedClassSingleton.Use_D = true;
                loggerNestedClassSingleton.WriteProtocol("logger created", "LoggerNestedClassSingleton", "");
            }
            using (LoggerLazyLoadSingleton loggerLazyLoadSingleton = LoggerLazyLoadSingleton.Instance)
            {
                loggerLazyLoadSingleton.Use_D = true;
                loggerLazyLoadSingleton.WriteProtocol("logger created", "LoggerLazyLoadSingleton", "");
            }
        }
    }
}
