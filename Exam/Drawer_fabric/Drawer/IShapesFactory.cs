﻿namespace Drawer
{
    public interface IShapesFactory
    {
        IShape Create();
    }
}