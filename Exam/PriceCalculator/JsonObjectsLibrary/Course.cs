﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JsonObjectsLibrary
{
    public class Course : IProductItem
    {
        private int id;
        private string name;
        private double cost;
        private int minAge;

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public double Cost { get => cost; set => cost = value; }
        public int MinAge { get => minAge; set => minAge = value; }
    }
}
