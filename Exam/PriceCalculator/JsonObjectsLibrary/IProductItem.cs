﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JsonObjectsLibrary
{
    public interface IProductItem
    {
        int Id { get; set; }
        string Name { get; set; }
        double Cost { get; set; }
        int MinAge { get; set; }
    }
}
