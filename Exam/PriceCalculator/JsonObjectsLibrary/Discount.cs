﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JsonObjectsLibrary
{
    public class Discount
    {
        private double totalSum;
        private double discountPercent;

        public double TotalSum { get => totalSum; set => totalSum = value; }
        public double DiscountPercent { get => discountPercent; set => discountPercent = value; }
    }
}
