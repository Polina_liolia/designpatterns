﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JsonObjectsLibrary
{
    public interface IBuilder
    {
        void AddPart(IProductItem item);
        void RemovePart(IProductItem item);
        void FilterCourses(IList<IProductItem> itemsAvailable);
        IProduct  GetResult();
    }
}
