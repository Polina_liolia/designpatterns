﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JsonObjectsLibrary
{
    public class AllCourses
    {
        private Course[] courses;
        private Discount[] discountRange;

        public Course[] Courses { get => courses; set => courses = value; }
        public Discount[] DiscountRange { get => discountRange; set => discountRange = value; }
    }
}
