﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JsonObjectsLibrary
{
    public class CoursesOrder : IProduct
    {
        private double discountPercent;
        public IList<IProductItem> Items { get; set; }
        public double TotalCost { get;  set; }
        public double DiscountSum { get; set; }
        public double TotalCostDiscount { get;  set; }

        public double DiscountPercent
        {
            get
            {
                return discountPercent;
            }
            set
            {
                if(value >= 0 && value < 100)
                {
                    discountPercent = value;
                    DiscountSum = TotalCost * discountPercent / 100;
                    TotalCostDiscount =TotalCost - DiscountSum;
                }
            }
        }


        public CoursesOrder()
        {
            Items = new List<IProductItem>();
        }

        public bool Add(IProductItem course)
        {
            bool added = false;
            if (!Items.Contains(course))
            {
                Items.Add(course);
                added = true;
                TotalCost += course.Cost;
            }
            return added;
        }

        public bool Remove(IProductItem course)
        {
            bool removed = false;
            if (Items.Contains(course))
            {
                Items.Remove(course);
                removed = true;
                TotalCost -= course.Cost;
            }
            return removed;
        }

        public void Filter(IList<IProductItem> coursesAvailable)
        {
            for (int i = 0; i < Items.Count; i++)
            {
                if (!coursesAvailable.Contains(Items[i]))
                {
                    Items.Remove(Items[i]);
                    i--;
                }
            }
        }
    }
}
