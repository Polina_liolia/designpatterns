﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JsonObjectsLibrary
{
    public class OrderBuilder : IBuilder
    {
        private IProduct coursesOrder;
        private Discount[] discountRange;


        public OrderBuilder(Discount[] discountRange)
        {
            this.coursesOrder = new CoursesOrder();
            this.discountRange = discountRange;
        }

        public void AddPart(IProductItem course)
        {
            coursesOrder.Add(course);
        }

        public void RemovePart(IProductItem course)
        {
            coursesOrder.Remove(course);
        }

        public void FilterCourses(IList<IProductItem> coursesAvailable)
        {
            coursesOrder.Filter(coursesAvailable);
        }

        private void setCurrentDiscount()
        {
            double discountPercent = 0;
            for (int i = discountRange.Length - 1; i >= 0; i--)
            {
                if (discountRange[i].TotalSum <= coursesOrder.TotalCost)
                {
                    discountPercent = discountRange[i].DiscountPercent;
                    break;
                }
            }
            coursesOrder.DiscountPercent = discountPercent;
        }

        public IProduct GetResult()
        {
            setCurrentDiscount();
            return coursesOrder;
        }

        
    }
}
