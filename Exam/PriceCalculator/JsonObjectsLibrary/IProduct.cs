﻿using System.Collections.Generic;

namespace JsonObjectsLibrary
{
    public interface IProduct
    {
        IList<IProductItem> Items { get; set; }
        bool Add(IProductItem productItem);
        bool Remove(IProductItem productItem);
        void Filter(IList<IProductItem> productItemsAvailable);
        double TotalCost { get; set; }
        double DiscountSum { get; set; }
        double TotalCostDiscount { get;  set; }
        double DiscountPercent { get; set; }
    }
}
