﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.ServiceModel.Web;
using JsonObjectsLibrary;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net;
using System.Diagnostics;
using Newtonsoft.Json;
using System.ComponentModel;

namespace PriceCalculator
{
    public class Calculator : INotifyPropertyChanged
    {
        private IBuilder builder;
        public IBuilder Builder { get => builder; set => builder = value; }

        private IProductItem[] allCourses;
        private IList<IProductItem> CoursesAvailable;

        private int studentAge;
        public event PropertyChangedEventHandler PropertyChanged;
        public IProduct OrderCompleted { get; private set; }

        public int StudentAge
        {
            get
            {
                return studentAge;
            }
            set
            {
                studentAge = value;
                filterCourses();
                OrderCompleted = Builder.GetResult();
                NotifyPropertyChanged("OrderCompleted");
            }
        }

       

        public Calculator()
        {
            this.studentAge = 0; //default
            CoursesAvailable = new List<IProductItem>();
        }


       public IProductItem GetAvailableItem(int id)
        {
            return CoursesAvailable.FirstOrDefault(c => c.Id == id);
        }

        private void filterCourses()
        {
            CoursesAvailable = new List<IProductItem>();
            if (!(allCourses is null))
            {
                foreach (IProductItem course in allCourses)
                {
                    if (course.MinAge < studentAge)
                    {
                        CoursesAvailable.Add(course);
                    }
                }
                Builder.FilterCourses(CoursesAvailable);
                NotifyPropertyChanged("CoursesAvailable");
            }
        }

        #region Get data from JSON
        public async Task CoursesRequestAsync()
        {
            HttpClient client = new HttpClient();
            //getting response json data from post request to server:
            HttpResponseMessage response = await client.PostAsync("http://www.alltires.somee.com/home/Getcourses", new StringContent(""));
            try
            {
                response.EnsureSuccessStatusCode();
                string responseBodyJson = await response.Content.ReadAsStringAsync();
                //parsing JSON gata:
                AllCourses CoursesData = JsonConvert.DeserializeObject<AllCourses>(responseBodyJson);
                allCourses = CoursesData.Courses;
                Builder = new OrderBuilder(CoursesData.DiscountRange);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }
        #endregion

        #region INotifyPropertyChanged
        public void NotifyPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
        #endregion

    }
}
