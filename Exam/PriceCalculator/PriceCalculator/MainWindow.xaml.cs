﻿using JsonObjectsLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PriceCalculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Calculator calculator = new Calculator();
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = calculator;
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            await calculator.CoursesRequestAsync();
        }

        private void rb_age_Checked(object sender, RoutedEventArgs e)
        {
            if (sender is RadioButton)
            {
                RadioButton rb = sender as RadioButton;
                if (rb != null)
                {
                    int age = 0;
                    age = GetSelectedAge(rb);
                    calculator.StudentAge = age;
                }
            }
        }

        private static int GetSelectedAge(RadioButton rb)
        {
            int age = 0;
            switch (rb.Name)
            {
                case "rb_10_12": { age = 10; break; }
                case "rb_13_16": { age = 13; break; }
                case "rb_17_19": { age = 17; break; }
                case "rb_20_25": { age = 20; break; }
                case "rb_25_35": { age = 25; break; }
                case "rb_36": { age = 36; break; }
            }
            return age;
        }

        private void chb_Checked(object sender, RoutedEventArgs e)
        {
            if (sender is CheckBox)
            {
                CheckBox chbox = sender as CheckBox;
                if (chbox != null)
                {
                    IProductItem course = GetSelectedCourse(chbox);
                    if (!(course is null))
                        calculator.Builder.AddPart(course);
                }
            }
        }

        private void chb_Unchecked(object sender, RoutedEventArgs e)
        {
            if (sender is CheckBox)
            {
                CheckBox chbox = sender as CheckBox;
                if (chbox != null)
                {
                    IProductItem course = GetSelectedCourse(chbox);
                    if (!(course is null))
                        calculator.Builder.RemovePart(course);
                }
            }
        }

        private IProductItem GetSelectedCourse(CheckBox chbox)
        {
            IProductItem course = null;
            switch (chbox.Name)
            {
                case "chb_C_plus": { course = calculator.GetAvailableItem(0); break; }
                case "chb_Csharp_DotNet": { course = calculator.GetAvailableItem(1); break; }
                case "chb_Java": { course = calculator.GetAvailableItem(2); break; }
                case "chb_ADO": { course = calculator.GetAvailableItem(3); break; }
                case "chb_UI": { course = calculator.GetAvailableItem(4); break; }
                case "chb_markup": { course = calculator.GetAvailableItem(5); break; }
                case "chb_Photoshop": { course = calculator.GetAvailableItem(6); break; }
                case "chb_XML": { course = calculator.GetAvailableItem(7); break; }
                case "chb_msOffice": { course = calculator.GetAvailableItem(8); break; }
                case "chb_Composition": { course = calculator.GetAvailableItem(9); break; }
            }
            return course;
        }
    }
}
