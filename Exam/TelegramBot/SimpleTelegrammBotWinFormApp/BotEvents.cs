﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTelegrammBotWinFormApp
{
    public enum BotEvents
    {
        Start,
        StartAccepted,
        StartRejected,
        Scissors,
        Stone,
        Paper,
        Undefined
    }
}
