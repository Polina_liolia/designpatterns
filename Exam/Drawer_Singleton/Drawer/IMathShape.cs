﻿namespace Drawer
{
    public interface IMathShape 
    {
       double calcS();
       double calcP();
    }
}