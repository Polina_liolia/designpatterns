﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Drawer
{
    //ответственность - описывает понятие прямоугольник
    //центр, ширина и высота
    public class Rectangle : Square, IShape
    {
        private int height;
        public Rectangle(DrawerSingleton drawer, int x, int y, int width, int height) : base(drawer, x, y, width)
        {
            this.height = height;
            TypeShape = "RECTANGLE";
        }

        public void Draw() //зависит от Graphics через свойство в  MyDrawer, которое его содержит
        {
            drawer.Graphics.DrawRectangle(Pens.Red, (x - width / 2), (y - width / 2), width, height);
        }

        public double calcS()
        {
            return width * height;
        }

        public double calcP()
        {
            return (width + height) * 2;
        }
    }
}
