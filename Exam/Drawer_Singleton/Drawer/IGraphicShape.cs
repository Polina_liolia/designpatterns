﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Drawer
{
    public interface IGraphicShape
    {
       
        void Draw();
    }
}
