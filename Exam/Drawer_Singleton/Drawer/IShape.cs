﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Drawer
{
    //interfase for access throu factory  only
    public interface IShape : IGraphicShape, IMathShape
    {
        string TypeShape { get; set; }
    }
}
